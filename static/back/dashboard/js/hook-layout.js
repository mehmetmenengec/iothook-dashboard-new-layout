$(window).on('load', function() {
    $(".preloader").fadeOut(200);
    $(".iothook-dashboard").fadeIn(300);
});

$(document).ready(function(){
    $('[data-tooltip="tooltip"]').tooltip();
});

$(document).ready(function() {
    var stickyNav = function(){
        if ($('.iothook-dashboard').scrollTop() > 0) {
            $('.dm-nav').addClass('nav-sticky');
            $('.dashboard-main').addClass('pt-5');
        } else {
            $('.dm-nav').removeClass('nav-sticky');
            $('.dashboard-main').removeClass('pt-5');
        }
    };
    stickyNav();
    $('.iothook-dashboard').scroll(function() {
        stickyNav();
    });
});